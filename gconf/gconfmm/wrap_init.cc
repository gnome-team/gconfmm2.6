
#include <glib.h>

// Disable the 'const' function attribute of the get_type() functions.
// GCC would optimize them out because we don't use the return value.
#undef  G_GNUC_CONST
#define G_GNUC_CONST /* empty */

#include <gconfmm/wrap_init.h>
#include <glibmm/error.h>
#include <glibmm/object.h>

// #include the widget headers so that we can call the get_type() static methods:

#include "changeset.h"
#include "client.h"
#include "entry.h"
#include "schema.h"
#include "value.h"

extern "C"
{

//Declarations of the *_get_type() functions:

GType gconf_client_get_type(void);

//Declarations of the *_error_quark() functions:

GQuark gconf_error_quark(void);
} // extern "C"


//Declarations of the *_Class::wrap_new() methods, instead of including all the private headers:

namespace Gnome { namespace Conf {  class Client_Class { public: static Glib::ObjectBase* wrap_new(GObject*); };  } }

namespace Gnome { namespace Conf { 

void wrap_init()
{
  // Register Error domains:
  Glib::Error::register_domain(gconf_error_quark(), &::Gnome::Conf::Error::throw_func);

// Map gtypes to gtkmm wrapper-creation functions:
  Glib::wrap_register(gconf_client_get_type(), &Gnome::Conf::Client_Class::wrap_new);

  // Register the gtkmm gtypes:
  Gnome::Conf::Client::get_type();

} // wrap_init()

} //Conf
} //Gnome


