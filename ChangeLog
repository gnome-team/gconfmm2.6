2.28.3:

2011-10-18  Kalev Lember  <kalevlember@gmail.com>

	Avoid dependency on doc-install.pl. Bug #648860.

	* configure.ac: Distribute doc-install.pl with the tarball to
	avoid depending on mm-common for tarball builds.

2.28.2:

2010-06-04  Murray Cumming  <murrayc@murrayc.com>

	Fixed broken pkg-config file.

	* gconf/gconfmm.pc.in: Make this like the pangomm one. It had
    unreplaced variable names in the generated .pc files, making the 2.28.1
    release useless.

2010-06-04  Murray Cumming  <murrayc@murrayc.com>

	Entry: Use gconf_entry_copy() instead of implementing our own.

	* gconf/src/entry.[hg|ccg]: Update the code now (well, since 2003, but
    I just noticed) that gconf_entry_copy() is available.

2.28.1:

2010-06-03  Murray Cumming  <murrayc@murrayc.com>

	Fix compiler warnings.

	* gconf/gconfmm/callback.cc:
	* gconf/src/changeset.ccg: Comment out unused method parameters.
	* gconf/src/entry.hg: Use gconf_entry_unref() instead of gconf_entry_free(),
	which is deprecated, though one just calls the other, and neither are really
	documented. It's still not a registered boxed type.

2010-06-03  Murray Cumming  <murrayc@murrayc.com>

	Build the documentation.

	* docs/Makefile.am: Make this like the other mm-common-using modules
	too.

2010-06-03  Murray Cumming  <murrayc@murrayc.com>

	Build: Convert to using mm-common.

2010-06-03  Murray Cumming  <murrayc@murrayc.com>

	Added doxygen main page, though it is not used yet.

	* gconf/gconfmm.h: Added a main page, though this does not seem to be
	used yet, probably because gconfmm does not use mm-common yet.

2.28.0:

2009-03-27  Murray Cumming  <murrayc@murrayc.com>

	* gconf/src/client.ccg:
	* gconf/src/value.ccg: Add some static g_conf_*_get_type() functions that
	call gconf_*_get_type() to fix the build because gmmproc now guesses the
	prefix wrong, because we made it guess other prefixes better.

2.26.0:

2009-03-16  Murray Cumming  <murrayc@murrayc.com>

	* configure.in: Increased version number to match GNOME 2.26.

2009-01-24  Deng Xiyue  <manphiz@gmail.com>

	* Change license header to mention Lesser General Public License
	version 2.1 instead of Library General Public License, to be
	consistent with COPYING.

2008-12-12  Przemysław Grzegorczyk  <pgrzegorczyk@gmail.com>

	* gconf/src/client: Cleaned up glib includes to use
	only toplevel headers.
	Bug #564214

2.24.0:

2008-09-21  Murray Cumming  <murrayc@murrayc.com>

	* configure.in: Updated version to match gnome 2.24.

2.23.1:

2008-08-25  Murray Cumming  <murrayc@murrayc.com>

	* docs/reference/Doxyfile.in: Added newin2p* ALIASES, as in
	gtkmm, to mark new API.
	* gconf/src/client.hg: recursive_unset(): Added a suitable
	default value for the flags parameter.
	Added notify().
	Used @newin2p24 in the documentation.

2008-08-14  Josephine Hsu  <w30372@motorola.com>

	* gconf/src/gconf_methods.defs
	* gconf/src/value.hg
	* gconf/src/client.hg
	* tools/m4/convert_gconfmm.m4 :
	Added UnsetFlags and Client::recursive_unset().
	Bug #549158.

2008-01-28  Murray Cumming  <murrayc@murrayc.com>

	* gconf/src/client.hg: Added a string conversion for the signal,
	because this is no longer in the .m4 file installed by glibmm.

2.22.0:

2007-12-31  Armin Burgmeier  <armin@openismus.com>

	* configure.in: Detect whether building for Windows (copied from
	glibmm) and added AC_LIBTOOL_WIN32_DLL to create a shared library.

	* build_shared/Makefile_build.am_fragment: Link with  -no-undefined on
	Windows, otherwise libtool does not create a DLL because undefined
	symbols are not allowed in Windows DLLs.

2.20.0:

2007-09-09  Jonathon Jongsma  <jjongsma@gnome.org>

	* docs/reference/Doxyfile.in: Add defines to PREDEFINED so that documentation
	is built wihout hiding optional API, such as use of exceptions.

2.18.1:

2007-09-10  Martin Ejdestig  <marejde@gmail.com>

	* gconf/gconfmm/setinterface.h: Use
	#pragma GCC system_header
	to supress warnings about the lack of a virtual destructor.
	We cannot add the virtual destructor (not a big problem in this
	case anyway) without breaking ABI, and we do not want to stop
	applications from building while -Werror is enabled.
	Bug #460969

2007-04-24  Yselkowitz <yselkowitz@users.sourceforge.net>

        * scripts/macros.m4: Check for both m4 and M4 in the GNU m4 output,
        to fix the build on some platforms.
        Bug #423990

2.18.0:

2007-03-09  Murray Cumming  <murrayc@murrayc.com>

	* Increased version for the GNOME 2.18 release.

2.16.0:

2.14.2:

2006-06-13  Jonathon Jongsma  <jonathon.jongsma@gmail.com>

	* examples/client/main.cc: fixed syntax error in the catch block.  Thanks to
	Elijah Newren (fixes bug #344792)

2.14.1:

2006-06-05  Murray Cumming  <murrayc@murrayc.com>

	* gconf/gconfmm/setinterface.cc:
	* gconf/gconfmm/setinterface.h:
	* gconf/src/changeset.ccg:
	* gconf/src/changeset.hg: Add #ifdefed versions of the API
	for when --enable-api-exceptions=no was used with glibmm,
	adding the Glib::Error parameter to methods.
	* examples/client/main.cc:
	* examples/client/window_test.cc: Added #ifdefed alternative code.

2.14.0:

2006-03-16  Murray Cumming  <murrayc@murrayc.com>

	* NEWS:
	* configure.in: Increased version for GNOME 2.14.

2005-12-06   Jonathon Jongsma <jonathon.jongsma@gmail.com>

	* configure.in: Look for gmmproc in generic libdir
	directory, because that is not always lib/. This
	fixes the build on some 64-bit systems.

2005-11-28  Murray Cumming  <murrayc@murrayc.com>

	* gconf/gconfmm/value_listhelpers.h: Remove extra
	template<> to allow compilation on IBM (AIX), Tru64, and
	SGI C++ compilers. Bug #321967 with patch by
	The Written Word.

2.12.0:

2005-04-28  Stanislav Brabec  <sbrabec@suse.cz>

	* gconf/src/client.ccg: Use platform dependent GPOINTER_TO_INT for
	casts.

2005-04-24  Murray Cumming <murrayc@murrayc.com>

	* gconf/src/client.ccg: Added casts for gcc 4.0 on AMD64, with
	comments. Bug #172387 and patch from Bradley Bell.

2.10.0:

2005-03-03  Murray Cumming  <murrayc@murrayc.com>

	* gconf/src/client.hg: BasicTypeTraits: Allocate a new pointer to the
	type. This will be used by float lists.
	set_float_list, get_float_list: Use doubles, not floats, because
	that is what we use elsewhere, and they are theoretically equivalent.

2005-02-15  Murray Cumming  <murrayc@murrayc.com>

	* gconf/src/client.hg: BasicTypeTraits: Remove extra prefix, to avoid
	warning. Do not delete anything in the int and bool specializations.

2005-02-14  Murray Cumming  <murrayc@murrayc.com>

	* gconf/src/client.hg: Added int and bool template specializations of
	BasicTypes<> because gconf needs us to use GPOINTER_TO_INT and
	GINT_TO_POINTER with gconf_client_set_list() when using
	GCONF_VALUE_INT and GCONF_VALUE_BOOL. Bug #63585 from Bryan Forbes.
	We are still not sure what to do for GCONF_VALUE_FLOAT.

2005-02-03  Murray Cumming  <murrayc@murrayc.com>

	* gconf/src/client.hg: BasicTypeTraits::release_c_type(): Remove
	unnecessary prefix, to fix warning.

2.9.1:

2005-01-10  Murray Cumming  <murrayc@murrayc.com>

	* gconf/src/client.[hg|ccg]: set_int_list(), set_bool_list(),
	set_float_list(): Use a SListHandle_Value with a special Traits,
	because the default Traits for basic types seem to be for
	ArrayHandles, rather than GLists which store pointers rather than
	values. However, this does not work yet - the values in the GList are
	nonsense.

2005-01-10  Bryan Forbes  <bryan@reigndropsfall.net>

	* gconf/src/client.[hg|ccg]: changed SListHandle_Value* to SListHandle<*>
	for set_*_list() methods.
	* examples/client/window_test.cc: commented out the int list example because
	it doesn't compile now.

2005-01-10  Bryan Forbes  <bryan@reigndropsfall.net>

	* gconf/src/client.[hg|ccg]: renamed set() methods for lists to
	set_[type]_list().
	* examples/client/window_test.cc: added two list setting examples in the
	constructor to show that they don't work.

2.9.1:

2005-01-06  Bryan Forbes  <bryan@reigndropsfall.net>

	* gconf/src/client.[hg|ccg]: added set() methods for lists.

This is the HEAD branch, for gconfmm 2.9/2.10.

2.8.1:

2004-11-28  Murray Cumming  <murrayc@murrayc.com>

	* gconf/src/src/value.hg, client.hg: Removed extra ;s which cause
	warnings with g++ 3.4.

2.8.0:

2004-09-12  Murray Cumming  <murrayc@murrayc.com>

	* Increased version for GNOME 2.8 release version.

2.6.1:

2004-04-21  Daniel Elstner  <daniel.elstner@gmx.net>

	* tools/m4/convert_gconfmm.m4: Make _CONVERSION() from GConfValue*
	to const Value& take a copy.  This fixes a crash caused by double
	destruction of the value argument to Client::signal_value_changed().
	Changing a global conversion for this is kinda fragile, but that's
	a design problem of gtkmmproc; fortunately no other code is using
	this particular conversion.

2.6.0:

2004-04-07  Alexander Nedotsukov <bland@FreeBSD.org>

	* Fixed installation directory messed up after 2.0 -> 2.6 change.

2.5.1:

2004-02-13  Murray Cumming  <murrayc@murrayc.com>

	* Updated for latest libsigc++ 2 API.

2.5.0:

2003-11-05  Murray Cumming  <murrayc@usa.net>

	* configure.in: Fixed typo so that it can find the installed gmmproc
	and actually build.

2003-11-02  Murray Cumming  <murrayc@usa.net>

	* This is now gconfmm 2.5, which will become gconfmm 2.6. It uses
	glibmm 2.4 and is parallel-installable.

Branched for HEAD (gconfmm 2.5/2.6) and gnome-2-4.

2003-09-24  Eric Bourque <ericb@computer.org>

 	* gconfmm.spec.in - new file

2003-09-04  Bradley Bell  <btb@debian.org>

	* Makefile.am, configure.in, docs: Generate reference docs using
	doxygen

2.0.1:

2003-01-27  Ole Laursen  <olau@hardworking.dk>

	* gconf/gconfmm/callback.cc (call): Instruct the Entry to copy the
	C object to prevent double destruction. Fixes bug #104181.

2.0.0:

2002-12-10  Andreas Holzmann  <Andreas.Holzmann@epost.de>

 	* gconf/src/client.[ccg|hg]: added get_entry() method that uses
 	the default locale.

2002-12-10  Andreas Holzmann  <Andreas.Holzmann@epost.de>

	* gconf/src/changeset.[ccg|hg]: made some get methods const.
	* gconf/src/client.[ccg|hg]: dito.
	* gconf/src/schema.hg: dito.

2002-12-09  Murray Cumming  <murrayc@usa.net>

	* Increased version number to 2.0.0 and renamed library from 1.3
	to 2.0

2002-11-26  Bassoukos Anastasios  <abas@frodo>

	* gconf/src/schema.*, gconf/src/entry.*: removed obsolete
	constructors.

1.3.10:

2002-11-21  Bassoukos Tassos  <abas@aix.meng.auth.gr>

	* gconf/src/value.hg, gconf/src/value.ccg: moved here from
	gconf/gconfmm/value.cc and value.h. Updated to use the new
	SListHandle for lists and _WRAP_METHOD. Documented.
	* gconf/src/client.hg, gconf/src/client.ccg: updated to use
	_WRAP_METHOD with error throw, Documented.
	* gconf/gconfmm/list_conversion.h: removed file.
	* gconf/src/Makefile_list_of_hg.am_fragment: added new files here.
	* gconf/gconfmm/value_listhelpers.h: new file, from list_helpers.h.
	Modified to use the SListHandle templates. Contains specializations
	needed for correct interface with GConfValues.
	* gconf/gconfmm/setinterface.h, gconf/gconfmm/setinterface.cc:
	new files, common setter interface for Gnome::Conf::Client and
	Gnome::Conf::ChangeSet.
	* gconf/src/changeset.hg, gconf/src/changeset.ccg: new files,
	wrappers for GConfChangeSet.
	* gconf/gconfmm/Makefile.am: removed generated files from sources.

2002-11-20  Bassoukos Tassos  <abas@aix.meng.auth.gr>

 	* gconf/src/entry.hg, gconf/src/entry.ccg: moved here from
 	gconf/gconfmm/entry.cc, gconf/gconfmm/entry.h, updated to use the
 	m4 code generation macros. TODO: docs.
 	* gconf/src/schema.hg, gconf/src/schema.ccg: as above for
 	gconf/gconfmm/schema.cc and schema.h
 	* tools/m4/convert_gconfmm.m4: added more conversion macros,
 	including conversions for enumerations.
 	* gconf/src/Makefile_list_of_hg.am_fragment,

2002-11-21  Murray Cumming  <murrayc@usa.net>

	* gconf/gconfmm/Makefile: Distribute list_conversions.h

2002-11-19  Bassoukos Tassos  <abas@aix.meng.auth.gr>

	* gconf/src/gconf_methods.defs: updated using CVS h2defs.py and
	current headers; gconf-listeners.h is not included (backend only).
	Removed "is-constructor-of" attributes.

2002-11-09  Murray Cumming  <murrayc@usa.net>

	* gconf/gconfmm/Makefile.am: The private list_conversion.h header file
	is no longer installed. Suggested by Tasss Bassoukos

2002-11-09  Murray Cumming  <murrayc@usa.net>

	* gconf/gconfmm/list_conversion.h: Used const keyword to make
	ownership of GSLists more obvious.

2002-11-08  Tasss Bassoukos <abas@aix.meng.auth.gr>

	* gconf/gconfmm/list_conversion.h: The list conversion functions
	from GConfValue were incorrectly freeing GConfValues.

2002-11-04  Tasss Bassoukos <abas@aix.meng.auth.gr>

	* gconf/gconfmm/value.cc Value::get_list_type() was missing.

2002-11-05  Murray Cumming  <murrayc@usa.net>

	* gconf/gconfmm/value.cc: Value::Value(): don't call gconf_value_new()
	with invalid type for default constructor.

2002-09-20  Murray Cumming  <murrayc@usa.net>

	* Hid CallbackHolder from reference documentation.

1.3.9:

	* Updated for gtkmm2.
1.3.8:

2002-08-25  Murray Cummign  <murrayc@usa.net>

	* configure.in: Generated -uninstalled.pc for use with gnomemm-all.

1.3.7:

2002-07-19  Murray Cumming  <murrayc@usa.net>

	* gconf/Makefile.am: Corrected installation of gconfmm.h header.

1.3.6:

2002-07-09  Abel Cheung  <maddog@linux.org.hk>

	* tools/m4/Makefile.am: Corrected m4 install destination, was
	conflicting with libgnomemm counterpart.
	* tools/Makefile.am, tools/m4/Makefile.am: Bundle various Makefile
	fragments too, automake will fail without them.

1.3.5:

2002-06-23  Murray Cumming  <murrayc@usa.net>

	* Rebuilt for latest gtkmm.

1.3.4:

2002-06-16  Murray Cumming  <murrayc@usa.net>

	* examples/client: Changed use of Box::pack_start() to use the new
	enum parameter.

2002-05-14  Daniel Elstner  <daniel.elstner@gmx.net>

	* gconf/gconfmm/entry.cc, gconf/gconfmm/value.cc: Remove default
	method arguments from the implementation prototype -- gcc-3.1
	doesn't like them at all.

1.3.3:

2002-05-01  Murray Cumming  <murrayc@usa.net>

	* Client now uses namespace enums.

2002-04-23  Murray Cumming  <murrayc@usa.net>

	* Generated sources are now distributed. build_shared/*.am_fragment
	files were copied from libgnomecanvasmm.

2002-04-21  Murray Cumming  <murrayc@usa.net>

	* gconf/src/client.hg: Changed _CLASS_GERROR to _WRAP_GERROR, for
	recent gtkmm change.

1.3.2:

2002-04-01  Murray Cumming  <murrayc@usa.net>

	* Use new _DEFS() format in .hg files.

1.3.1:

2002-03-10  Murray Cumming  <murrayc@usa.net>

	* Changed signal args to C++ types.

cd 2002-03-06  Murray Cumming  <murrayc@usa.net>

	* Gnome::Conf::Client methods can now throw Gnome::Conf::Error
	exceptions, using the glibmm Glib::Error system. Therefore the
	get_last_error() method has now been removed.

2002-02-24  Murray Cumming  <murrayc@usa.net>

	* Corrected use of $() to @@ in pkg-config .pc.in file.
	(Rick L Vinyard Jr)

gconfmm 1.3.0:

2002-02-17  Murray Cumming  <murrayc@usa.net>

	* Changed namespace from GConf to Gnome::Conf, to make it more clearly
	a part of gnomemm. GConf does not depend on much of GNOME, but it is
	part of GNOME.
	* Added Gnome::Conf::init().
	* Added the example from the old gconfmm, and updated it for gtkmm2
	and gconfmm2.

2002-01-28  ERDI Gergo  <cactus@cactus.rulez.org>

	* gconf/src/client.hg: Use std::pair<> for set/get_pair API

001-06-17   Murray Cumming  <murrayc@usa.net>
        * Used notify_add in example, like in GConf example,
          so that two instances of the example will interact.

2001-06-16  Murray Cumming  <murrayc@usa.net>
        * gconf--.h is now installed.
        * typo in gconf--.cc fixed, so gconfmm code compiles without
          the 'undefined symbol' error.
        * External sample. If I'd done that before then these errors
          wouldn't have been there.

2001-04-24  Murray Cumming  <murrayc@usa.net>

	* GConfClient and Value methods that deal with GSList* args are
          now wrapped with std::list args. There's a lot of very
          unpleasant and repetitive code in list_conversion.h for this.
          I have a feeling that this could be simplified with the use
          of template specialization but I can't get that working.

2001-04-18  Murray Cumming  <murrayc@usa.net>

	* 'make install' now installs headers in include/gconf-- instead
          of include.
        * header guards now prefixed with GCONFMM instead of GTKEXTRAMM.
