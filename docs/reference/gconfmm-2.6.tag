<?xml version='1.0' encoding='ISO-8859-1' standalone='yes' ?>
<tagfile>
  <compound kind="page">
    <name>index</name>
    <title>gconfmm Reference Manual</title>
    <filename>index</filename>
    <docanchor file="index">basics</docanchor>
    <docanchor file="index">description</docanchor>
  </compound>
  <compound kind="group">
    <name>gconfmmEnums</name>
    <title>gconfmm Enums and Flags</title>
    <filename>group__gconfmmEnums.html</filename>
    <member kind="enumeration">
      <name>ClientErrorHandlingMode</name>
      <anchorfile>group__gconfmmEnums.html</anchorfile>
      <anchor>ga45912783c88c883ee158c6d60dc6f2c4</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>CLIENT_HANDLE_NONE</name>
      <anchorfile>group__gconfmmEnums.html</anchorfile>
      <anchor>gga45912783c88c883ee158c6d60dc6f2c4a24c47540bfbad672a784771a4ad56198</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>CLIENT_HANDLE_UNRETURNED</name>
      <anchorfile>group__gconfmmEnums.html</anchorfile>
      <anchor>gga45912783c88c883ee158c6d60dc6f2c4a9cc53078f3a17570e70ba162a544c005</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>CLIENT_HANDLE_ALL</name>
      <anchorfile>group__gconfmmEnums.html</anchorfile>
      <anchor>gga45912783c88c883ee158c6d60dc6f2c4a5c296f5df63553033c4fabffbab947b5</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <name>ClientPreloadType</name>
      <anchorfile>group__gconfmmEnums.html</anchorfile>
      <anchor>gad0575a00dcfc2a15d1b15e109704e99a</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>CLIENT_PRELOAD_NONE</name>
      <anchorfile>group__gconfmmEnums.html</anchorfile>
      <anchor>ggad0575a00dcfc2a15d1b15e109704e99aa9df4a4767f939145fe5bf4931279aaf4</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>CLIENT_PRELOAD_ONELEVEL</name>
      <anchorfile>group__gconfmmEnums.html</anchorfile>
      <anchor>ggad0575a00dcfc2a15d1b15e109704e99aad125a56edc6d11449efde3c79a916d04</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>CLIENT_PRELOAD_RECURSIVE</name>
      <anchorfile>group__gconfmmEnums.html</anchorfile>
      <anchor>ggad0575a00dcfc2a15d1b15e109704e99aaf0fb8481b3a9f90b8b0fc7e1a5314731</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <name>ValueType</name>
      <anchorfile>group__gconfmmEnums.html</anchorfile>
      <anchor>ga20fb20bb54017f616a098ecec72927a1</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>VALUE_INVALID</name>
      <anchorfile>group__gconfmmEnums.html</anchorfile>
      <anchor>gga20fb20bb54017f616a098ecec72927a1a7b1172fee5c0abf35245e80a9073e3af</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>VALUE_STRING</name>
      <anchorfile>group__gconfmmEnums.html</anchorfile>
      <anchor>gga20fb20bb54017f616a098ecec72927a1aae76d0398a4e2c90cc5d2d259dd7c206</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>VALUE_INT</name>
      <anchorfile>group__gconfmmEnums.html</anchorfile>
      <anchor>gga20fb20bb54017f616a098ecec72927a1ac601268661a4b209772c8491ad768734</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>VALUE_FLOAT</name>
      <anchorfile>group__gconfmmEnums.html</anchorfile>
      <anchor>gga20fb20bb54017f616a098ecec72927a1a4a0d6433997a196c9f30b37f2d210a81</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>VALUE_BOOL</name>
      <anchorfile>group__gconfmmEnums.html</anchorfile>
      <anchor>gga20fb20bb54017f616a098ecec72927a1a63ce621ecdf749a8d38e7c40aca38205</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>VALUE_SCHEMA</name>
      <anchorfile>group__gconfmmEnums.html</anchorfile>
      <anchor>gga20fb20bb54017f616a098ecec72927a1aa8cd13c084706ce7c62e6d3602ab30e2</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>VALUE_LIST</name>
      <anchorfile>group__gconfmmEnums.html</anchorfile>
      <anchor>gga20fb20bb54017f616a098ecec72927a1ab3a76cc787b6aec2b0d2f2f7020de925</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>VALUE_PAIR</name>
      <anchorfile>group__gconfmmEnums.html</anchorfile>
      <anchor>gga20fb20bb54017f616a098ecec72927a1a2e5c07952d814f32a67bf0c8d8349153</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <name>UnsetFlags</name>
      <anchorfile>group__gconfmmEnums.html</anchorfile>
      <anchor>ga7f874efbebfca90d6d78bcad52068085</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>UNSET_INCLUDING_SCHEMA_NAMES</name>
      <anchorfile>group__gconfmmEnums.html</anchorfile>
      <anchor>gga7f874efbebfca90d6d78bcad52068085a3876c410b5af49e0466bf6abc058a389</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Glib::Error</name>
    <filename>classGlib_1_1Error.html</filename>
  </compound>
  <compound kind="class">
    <name>Glib::Object</name>
    <filename>classGlib_1_1Object.html</filename>
  </compound>
  <compound kind="namespace">
    <name>Glib</name>
    <filename>namespaceGlib.html</filename>
  </compound>
  <compound kind="namespace">
    <name>Gnome</name>
    <filename>namespaceGnome.html</filename>
    <namespace>Gnome::Conf</namespace>
  </compound>
  <compound kind="namespace">
    <name>Gnome::Conf</name>
    <filename>namespaceGnome_1_1Conf.html</filename>
    <class kind="class">Gnome::Conf::Error</class>
    <class kind="class">Gnome::Conf::Client</class>
    <class kind="class">Gnome::Conf::Entry</class>
    <class kind="class">Gnome::Conf::Schema</class>
    <class kind="class">Gnome::Conf::Value</class>
    <class kind="class">Gnome::Conf::ChangeSet</class>
    <class kind="class">Gnome::Conf::SetInterface</class>
    <member kind="typedef">
      <type>std::pair&lt; Value, Value &gt;</type>
      <name>ValuePair</name>
      <anchorfile>namespaceGnome_1_1Conf.html</anchorfile>
      <anchor>a53532813345d00a4ddcd4018ac33092d</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>std::pair&lt; ValueType, ValueType &gt;</type>
      <name>ValueTypePair</name>
      <anchorfile>namespaceGnome_1_1Conf.html</anchorfile>
      <anchor>a285693631ab6667a2bcf7a4ecbfd2808</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>sigc::slot&lt; void, guint, Entry &gt;</type>
      <name>Callback</name>
      <anchorfile>namespaceGnome_1_1Conf.html</anchorfile>
      <anchor>aa7f52de608e8ecf7c3385b55e9674e52</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <name>ClientErrorHandlingMode</name>
      <anchorfile>group__gconfmmEnums.html</anchorfile>
      <anchor>ga45912783c88c883ee158c6d60dc6f2c4</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>CLIENT_HANDLE_NONE</name>
      <anchorfile>group__gconfmmEnums.html</anchorfile>
      <anchor>gga45912783c88c883ee158c6d60dc6f2c4a24c47540bfbad672a784771a4ad56198</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>CLIENT_HANDLE_UNRETURNED</name>
      <anchorfile>group__gconfmmEnums.html</anchorfile>
      <anchor>gga45912783c88c883ee158c6d60dc6f2c4a9cc53078f3a17570e70ba162a544c005</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>CLIENT_HANDLE_ALL</name>
      <anchorfile>group__gconfmmEnums.html</anchorfile>
      <anchor>gga45912783c88c883ee158c6d60dc6f2c4a5c296f5df63553033c4fabffbab947b5</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <name>ClientPreloadType</name>
      <anchorfile>group__gconfmmEnums.html</anchorfile>
      <anchor>gad0575a00dcfc2a15d1b15e109704e99a</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>CLIENT_PRELOAD_NONE</name>
      <anchorfile>group__gconfmmEnums.html</anchorfile>
      <anchor>ggad0575a00dcfc2a15d1b15e109704e99aa9df4a4767f939145fe5bf4931279aaf4</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>CLIENT_PRELOAD_ONELEVEL</name>
      <anchorfile>group__gconfmmEnums.html</anchorfile>
      <anchor>ggad0575a00dcfc2a15d1b15e109704e99aad125a56edc6d11449efde3c79a916d04</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>CLIENT_PRELOAD_RECURSIVE</name>
      <anchorfile>group__gconfmmEnums.html</anchorfile>
      <anchor>ggad0575a00dcfc2a15d1b15e109704e99aaf0fb8481b3a9f90b8b0fc7e1a5314731</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <name>ValueType</name>
      <anchorfile>group__gconfmmEnums.html</anchorfile>
      <anchor>ga20fb20bb54017f616a098ecec72927a1</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>VALUE_INVALID</name>
      <anchorfile>group__gconfmmEnums.html</anchorfile>
      <anchor>gga20fb20bb54017f616a098ecec72927a1a7b1172fee5c0abf35245e80a9073e3af</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>VALUE_STRING</name>
      <anchorfile>group__gconfmmEnums.html</anchorfile>
      <anchor>gga20fb20bb54017f616a098ecec72927a1aae76d0398a4e2c90cc5d2d259dd7c206</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>VALUE_INT</name>
      <anchorfile>group__gconfmmEnums.html</anchorfile>
      <anchor>gga20fb20bb54017f616a098ecec72927a1ac601268661a4b209772c8491ad768734</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>VALUE_FLOAT</name>
      <anchorfile>group__gconfmmEnums.html</anchorfile>
      <anchor>gga20fb20bb54017f616a098ecec72927a1a4a0d6433997a196c9f30b37f2d210a81</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>VALUE_BOOL</name>
      <anchorfile>group__gconfmmEnums.html</anchorfile>
      <anchor>gga20fb20bb54017f616a098ecec72927a1a63ce621ecdf749a8d38e7c40aca38205</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>VALUE_SCHEMA</name>
      <anchorfile>group__gconfmmEnums.html</anchorfile>
      <anchor>gga20fb20bb54017f616a098ecec72927a1aa8cd13c084706ce7c62e6d3602ab30e2</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>VALUE_LIST</name>
      <anchorfile>group__gconfmmEnums.html</anchorfile>
      <anchor>gga20fb20bb54017f616a098ecec72927a1ab3a76cc787b6aec2b0d2f2f7020de925</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>VALUE_PAIR</name>
      <anchorfile>group__gconfmmEnums.html</anchorfile>
      <anchor>gga20fb20bb54017f616a098ecec72927a1a2e5c07952d814f32a67bf0c8d8349153</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <name>UnsetFlags</name>
      <anchorfile>group__gconfmmEnums.html</anchorfile>
      <anchor>ga7f874efbebfca90d6d78bcad52068085</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>UNSET_INCLUDING_SCHEMA_NAMES</name>
      <anchorfile>group__gconfmmEnums.html</anchorfile>
      <anchor>gga7f874efbebfca90d6d78bcad52068085a3876c410b5af49e0466bf6abc058a389</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>init</name>
      <anchorfile>namespaceGnome_1_1Conf.html</anchorfile>
      <anchor>ab69ddbb11efde1d782f8c3ef711d2e9b</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Gnome::Conf::Error</name>
    <filename>classGnome_1_1Conf_1_1Error.html</filename>
    <base>Glib::Error</base>
    <member kind="enumeration">
      <name>Code</name>
      <anchorfile>classGnome_1_1Conf_1_1Error.html</anchorfile>
      <anchor>af312d8c9808fa03be5a4b8caee06503b</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>SUCCESS</name>
      <anchorfile>classGnome_1_1Conf_1_1Error.html</anchorfile>
      <anchor>af312d8c9808fa03be5a4b8caee06503ba2ced2838a9c65c1142b85b83e6eaac97</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>NO_SERVER</name>
      <anchorfile>classGnome_1_1Conf_1_1Error.html</anchorfile>
      <anchor>af312d8c9808fa03be5a4b8caee06503ba4d6978f2eb9c0cb36e972db2a1fc5632</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>NO_PERMISSION</name>
      <anchorfile>classGnome_1_1Conf_1_1Error.html</anchorfile>
      <anchor>af312d8c9808fa03be5a4b8caee06503ba0df70d51f533b853e99ce21b389df262</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>BAD_ADDRESS</name>
      <anchorfile>classGnome_1_1Conf_1_1Error.html</anchorfile>
      <anchor>af312d8c9808fa03be5a4b8caee06503baebbae4092a594dad0fea032562649c73</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>PARSE_ERROR</name>
      <anchorfile>classGnome_1_1Conf_1_1Error.html</anchorfile>
      <anchor>af312d8c9808fa03be5a4b8caee06503bad7adf3e4e9df12a758d83072cdc241a1</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>CORRUPT</name>
      <anchorfile>classGnome_1_1Conf_1_1Error.html</anchorfile>
      <anchor>af312d8c9808fa03be5a4b8caee06503baaa0a80b9eb644d256f89f3d5a4d3701b</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>TYPE_MISMATCH</name>
      <anchorfile>classGnome_1_1Conf_1_1Error.html</anchorfile>
      <anchor>af312d8c9808fa03be5a4b8caee06503ba8dfdbb3a123a13886a01bb172f0ec10f</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>IS_DIR</name>
      <anchorfile>classGnome_1_1Conf_1_1Error.html</anchorfile>
      <anchor>af312d8c9808fa03be5a4b8caee06503baf5fd1cb4dca714ea1be6752b403e83e0</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>IS_KEY</name>
      <anchorfile>classGnome_1_1Conf_1_1Error.html</anchorfile>
      <anchor>af312d8c9808fa03be5a4b8caee06503bafafb827a4de7c454e7fe2b44f8a3b2ba</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>OVERRIDDEN</name>
      <anchorfile>classGnome_1_1Conf_1_1Error.html</anchorfile>
      <anchor>af312d8c9808fa03be5a4b8caee06503ba635c45a667c6e956bc219adf520b05df</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>OAF_ERROR</name>
      <anchorfile>classGnome_1_1Conf_1_1Error.html</anchorfile>
      <anchor>af312d8c9808fa03be5a4b8caee06503ba9c64816c3e559b228155f582aff894ca</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>LOCAL_ENGINE</name>
      <anchorfile>classGnome_1_1Conf_1_1Error.html</anchorfile>
      <anchor>af312d8c9808fa03be5a4b8caee06503bafb99a8b86efa39eefcdbd574cb562264</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>LOCK_FAILED</name>
      <anchorfile>classGnome_1_1Conf_1_1Error.html</anchorfile>
      <anchor>af312d8c9808fa03be5a4b8caee06503ba8382a418a950f3405734d7d1bca157b3</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>NO_WRITABLE_DATABASE</name>
      <anchorfile>classGnome_1_1Conf_1_1Error.html</anchorfile>
      <anchor>af312d8c9808fa03be5a4b8caee06503bab582d0ff085276dae091d37e52f5c606</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>IN_SHUTDOWN</name>
      <anchorfile>classGnome_1_1Conf_1_1Error.html</anchorfile>
      <anchor>af312d8c9808fa03be5a4b8caee06503ba6bb4fda686fd203fc627513f69826afb</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Error</name>
      <anchorfile>classGnome_1_1Conf_1_1Error.html</anchorfile>
      <anchor>aaa83b1b1508af15d49e54c117d052afa</anchor>
      <arglist>(Code error_code, const Glib::ustring &amp;error_message)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Error</name>
      <anchorfile>classGnome_1_1Conf_1_1Error.html</anchorfile>
      <anchor>aae45da62d76d83521b1db290ef35e127</anchor>
      <arglist>(GError *gobject)</arglist>
    </member>
    <member kind="function">
      <type>Code</type>
      <name>code</name>
      <anchorfile>classGnome_1_1Conf_1_1Error.html</anchorfile>
      <anchor>a2b4918d47c61734fa331b2aaf866fa3e</anchor>
      <arglist>() const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Gnome::Conf::Client</name>
    <filename>classGnome_1_1Conf_1_1Client.html</filename>
    <base>Glib::Object</base>
    <base>Gnome::Conf::SetInterface</base>
    <member kind="typedef">
      <type>Glib::SListHandle&lt; int, BasicTypeTraits&lt; int &gt; &gt;</type>
      <name>SListHandleInts</name>
      <anchorfile>classGnome_1_1Conf_1_1Client.html</anchorfile>
      <anchor>a58f35130189f26313b0b221b7147271f</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>Glib::SListHandle&lt; bool, BasicTypeTraits&lt; bool &gt; &gt;</type>
      <name>SListHandleBools</name>
      <anchorfile>classGnome_1_1Conf_1_1Client.html</anchorfile>
      <anchor>acf8b936341ce9f52dd0e1835727e75dd</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>Glib::SListHandle&lt; double, BasicTypeTraits&lt; double &gt; &gt;</type>
      <name>SListHandleFloats</name>
      <anchorfile>classGnome_1_1Conf_1_1Client.html</anchorfile>
      <anchor>a53c51bfee8f732a028035835019003ea</anchor>
      <arglist></arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~Client</name>
      <anchorfile>classGnome_1_1Conf_1_1Client.html</anchorfile>
      <anchor>a1bb77f2e39e1fe0fd51cf07150139eb1</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>GConfClient *</type>
      <name>gobj</name>
      <anchorfile>classGnome_1_1Conf_1_1Client.html</anchorfile>
      <anchor>a5df9a0136f78491379124d0bd8876075</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>const GConfClient *</type>
      <name>gobj</name>
      <anchorfile>classGnome_1_1Conf_1_1Client.html</anchorfile>
      <anchor>a0b2227668a2e934ecc264146b1587d50</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>GConfClient *</type>
      <name>gobj_copy</name>
      <anchorfile>classGnome_1_1Conf_1_1Client.html</anchorfile>
      <anchor>a0679c98477afcc10e83c65a3e78475a6</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>add_dir</name>
      <anchorfile>classGnome_1_1Conf_1_1Client.html</anchorfile>
      <anchor>a752d1a7e9172b4ba4d112594c2711dfb</anchor>
      <arglist>(const Glib::ustring &amp;dir, ClientPreloadType preload=CLIENT_PRELOAD_NONE)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>remove_dir</name>
      <anchorfile>classGnome_1_1Conf_1_1Client.html</anchorfile>
      <anchor>a41aaf6ebdcd026629e48446927d3666c</anchor>
      <arglist>(const Glib::ustring &amp;dir)</arglist>
    </member>
    <member kind="function">
      <type>guint</type>
      <name>notify_add</name>
      <anchorfile>classGnome_1_1Conf_1_1Client.html</anchorfile>
      <anchor>a46ae9fb669535fb2e9c33e71bd150871</anchor>
      <arglist>(const Glib::ustring &amp;namespace_section, Callback callback)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>notify_remove</name>
      <anchorfile>classGnome_1_1Conf_1_1Client.html</anchorfile>
      <anchor>ac56f16796ad1451452a4544ace2256de</anchor>
      <arglist>(guint cnxn)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>notify</name>
      <anchorfile>classGnome_1_1Conf_1_1Client.html</anchorfile>
      <anchor>ae5d23b18dfa9b2c25bcd39ca853ee3d0</anchor>
      <arglist>(const Glib::ustring &amp;key)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set_error_handling</name>
      <anchorfile>classGnome_1_1Conf_1_1Client.html</anchorfile>
      <anchor>ab40c988361a768719926ddc50e28115e</anchor>
      <arglist>(ClientErrorHandlingMode mode)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>clear_cache</name>
      <anchorfile>classGnome_1_1Conf_1_1Client.html</anchorfile>
      <anchor>a4555d99d897df46fbdd9e6dfaab12a7c</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>preload</name>
      <anchorfile>classGnome_1_1Conf_1_1Client.html</anchorfile>
      <anchor>a9b300092e1970808ceb364eac348f7a8</anchor>
      <arglist>(const Glib::ustring &amp;dirname, ClientPreloadType type)</arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>get</name>
      <anchorfile>classGnome_1_1Conf_1_1Client.html</anchorfile>
      <anchor>a059b6f4cd673d22e9c4f656904136b4d</anchor>
      <arglist>(const Glib::ustring &amp;key) const </arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>get_without_default</name>
      <anchorfile>classGnome_1_1Conf_1_1Client.html</anchorfile>
      <anchor>abb202018927c82c23c8d3c74843c4a41</anchor>
      <arglist>(const Glib::ustring &amp;key) const </arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>get_default_from_schema</name>
      <anchorfile>classGnome_1_1Conf_1_1Client.html</anchorfile>
      <anchor>aab8795987feaa0009244e3f91a574276</anchor>
      <arglist>(const Glib::ustring &amp;key) const </arglist>
    </member>
    <member kind="function">
      <type>Entry</type>
      <name>get_entry</name>
      <anchorfile>classGnome_1_1Conf_1_1Client.html</anchorfile>
      <anchor>a91173a8a764cfed8acccb048358d0199</anchor>
      <arglist>(const Glib::ustring &amp;key, bool use_schema_default=true) const </arglist>
    </member>
    <member kind="function">
      <type>Entry</type>
      <name>get_entry</name>
      <anchorfile>classGnome_1_1Conf_1_1Client.html</anchorfile>
      <anchor>a144f33d505eec0f62de39cb0c75762cb</anchor>
      <arglist>(const Glib::ustring &amp;key, const char *locale, bool use_schema_default=true) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>unset</name>
      <anchorfile>classGnome_1_1Conf_1_1Client.html</anchorfile>
      <anchor>ab6e48a6db99e2d472d97377e4e0505ba</anchor>
      <arglist>(const Glib::ustring &amp;key)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>recursive_unset</name>
      <anchorfile>classGnome_1_1Conf_1_1Client.html</anchorfile>
      <anchor>a1dadc3c5a7b48f16eb7e638b2a71bbba</anchor>
      <arglist>(const Glib::ustring &amp;key, UnsetFlags flags=UNSET_INCLUDING_SCHEMA_NAMES)</arglist>
    </member>
    <member kind="function">
      <type>Glib::SListHandle&lt; Entry &gt;</type>
      <name>all_entries</name>
      <anchorfile>classGnome_1_1Conf_1_1Client.html</anchorfile>
      <anchor>aae728903846aa0bd97494d61d63f166a</anchor>
      <arglist>(const Glib::ustring &amp;dir) const </arglist>
    </member>
    <member kind="function">
      <type>Glib::SListHandle&lt; Glib::ustring &gt;</type>
      <name>all_dirs</name>
      <anchorfile>classGnome_1_1Conf_1_1Client.html</anchorfile>
      <anchor>a9fe7c96a54aff4e11d015df784450b95</anchor>
      <arglist>(const Glib::ustring &amp;dir) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>suggest_sync</name>
      <anchorfile>classGnome_1_1Conf_1_1Client.html</anchorfile>
      <anchor>a949121403d46ca78268269f36d9575af</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>dir_exists</name>
      <anchorfile>classGnome_1_1Conf_1_1Client.html</anchorfile>
      <anchor>a040e0094d0ca80d5130c84cbf686564f</anchor>
      <arglist>(const Glib::ustring &amp;p1) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>key_is_writable</name>
      <anchorfile>classGnome_1_1Conf_1_1Client.html</anchorfile>
      <anchor>a1fff1b2a4972af1da815ba5f6e12b1b2</anchor>
      <arglist>(const Glib::ustring &amp;p1) const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>get_float</name>
      <anchorfile>classGnome_1_1Conf_1_1Client.html</anchorfile>
      <anchor>ac3d0a826114da44e878539af7340fe18</anchor>
      <arglist>(const Glib::ustring &amp;key) const </arglist>
    </member>
    <member kind="function">
      <type>gint</type>
      <name>get_int</name>
      <anchorfile>classGnome_1_1Conf_1_1Client.html</anchorfile>
      <anchor>a5f77cf1fccf4dc4436de8da18c3183f6</anchor>
      <arglist>(const Glib::ustring &amp;key) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>get_bool</name>
      <anchorfile>classGnome_1_1Conf_1_1Client.html</anchorfile>
      <anchor>ae8049b65be022921da9967da7cd3b5ff</anchor>
      <arglist>(const Glib::ustring &amp;key) const </arglist>
    </member>
    <member kind="function">
      <type>Glib::ustring</type>
      <name>get_string</name>
      <anchorfile>classGnome_1_1Conf_1_1Client.html</anchorfile>
      <anchor>a2dcccf695be17214feccc43bf07a8d0e</anchor>
      <arglist>(const Glib::ustring &amp;key) const </arglist>
    </member>
    <member kind="function">
      <type>Schema</type>
      <name>get_schema</name>
      <anchorfile>classGnome_1_1Conf_1_1Client.html</anchorfile>
      <anchor>ad7f53c7fd8bfdba3e005a126b6ca79f8</anchor>
      <arglist>(const Glib::ustring &amp;key) const </arglist>
    </member>
    <member kind="function">
      <type>SListHandle_ValueInt</type>
      <name>get_int_list</name>
      <anchorfile>classGnome_1_1Conf_1_1Client.html</anchorfile>
      <anchor>ab542ac8a24891b54c359334d7dd41810</anchor>
      <arglist>(const Glib::ustring &amp;key) const </arglist>
    </member>
    <member kind="function">
      <type>SListHandle_ValueBool</type>
      <name>get_bool_list</name>
      <anchorfile>classGnome_1_1Conf_1_1Client.html</anchorfile>
      <anchor>a1f181fc1330a7525bc0826a224982ad6</anchor>
      <arglist>(const Glib::ustring &amp;key) const </arglist>
    </member>
    <member kind="function">
      <type>SListHandle_ValueFloat</type>
      <name>get_float_list</name>
      <anchorfile>classGnome_1_1Conf_1_1Client.html</anchorfile>
      <anchor>af797b74761cb40e772c2c940bb8d3954</anchor>
      <arglist>(const Glib::ustring &amp;key) const </arglist>
    </member>
    <member kind="function">
      <type>SListHandle_ValueSchema</type>
      <name>get_schema_list</name>
      <anchorfile>classGnome_1_1Conf_1_1Client.html</anchorfile>
      <anchor>a683f2ff59f540652d20c465971f8e4d2</anchor>
      <arglist>(const Glib::ustring &amp;key) const </arglist>
    </member>
    <member kind="function">
      <type>SListHandle_ValueString</type>
      <name>get_string_list</name>
      <anchorfile>classGnome_1_1Conf_1_1Client.html</anchorfile>
      <anchor>a818df5ad84bd1df1cbe392cbedc57aeb</anchor>
      <arglist>(const Glib::ustring &amp;key) const </arglist>
    </member>
    <member kind="function">
      <type>ValuePair</type>
      <name>get_pair</name>
      <anchorfile>classGnome_1_1Conf_1_1Client.html</anchorfile>
      <anchor>a9485c82a4e959f328bdd8aa1b9155b0b</anchor>
      <arglist>(const Glib::ustring &amp;key, ValueTypePair types) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set</name>
      <anchorfile>classGnome_1_1Conf_1_1Client.html</anchorfile>
      <anchor>a44e92ef5dc82f7ca79fa0fac64f6bba9</anchor>
      <arglist>(const Glib::ustring &amp;key, int what)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set</name>
      <anchorfile>classGnome_1_1Conf_1_1Client.html</anchorfile>
      <anchor>a133cb49ee01abbe30f111b9c924367bb</anchor>
      <arglist>(const Glib::ustring &amp;key, bool what)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set</name>
      <anchorfile>classGnome_1_1Conf_1_1Client.html</anchorfile>
      <anchor>ada36f42a271508cf550033bd763afa14</anchor>
      <arglist>(const Glib::ustring &amp;key, double what)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set</name>
      <anchorfile>classGnome_1_1Conf_1_1Client.html</anchorfile>
      <anchor>ab263fb89054b60f5bd0a77dac6cbbba0</anchor>
      <arglist>(const Glib::ustring &amp;key, const Glib::ustring &amp;what)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set</name>
      <anchorfile>classGnome_1_1Conf_1_1Client.html</anchorfile>
      <anchor>aac43c01d77071452d4ce92e81b2d2eb6</anchor>
      <arglist>(const Glib::ustring &amp;key, const Schema &amp;what)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set</name>
      <anchorfile>classGnome_1_1Conf_1_1Client.html</anchorfile>
      <anchor>aef236807423ba2e0a7b0d5d30159d91c</anchor>
      <arglist>(const Glib::ustring &amp;key, const Value &amp;what)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set_int_list</name>
      <anchorfile>classGnome_1_1Conf_1_1Client.html</anchorfile>
      <anchor>a1258ba3c8548953e90be69c3f1815d4e</anchor>
      <arglist>(const Glib::ustring &amp;key, const SListHandleInts &amp;what)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set_bool_list</name>
      <anchorfile>classGnome_1_1Conf_1_1Client.html</anchorfile>
      <anchor>a162957be007eed3cdb638bca7ff9e4d6</anchor>
      <arglist>(const Glib::ustring &amp;key, const SListHandleBools &amp;what)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set_float_list</name>
      <anchorfile>classGnome_1_1Conf_1_1Client.html</anchorfile>
      <anchor>a80c0c8ebcb9154c3c32416124b0aa0e8</anchor>
      <arglist>(const Glib::ustring &amp;key, const SListHandleFloats &amp;what)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set_schema_list</name>
      <anchorfile>classGnome_1_1Conf_1_1Client.html</anchorfile>
      <anchor>a1fbd177777b4b4aa42543fd3537d9b4e</anchor>
      <arglist>(const Glib::ustring &amp;key, const Glib::SListHandle&lt; Schema &gt; &amp;what)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set_string_list</name>
      <anchorfile>classGnome_1_1Conf_1_1Client.html</anchorfile>
      <anchor>ad43cee2d9aebeafd02262477a3bd484a</anchor>
      <arglist>(const Glib::ustring &amp;key, const Glib::SListHandle&lt; Glib::ustring &gt; &amp;what)</arglist>
    </member>
    <member kind="function">
      <type>ChangeSet</type>
      <name>change_set_from_current</name>
      <anchorfile>classGnome_1_1Conf_1_1Client.html</anchorfile>
      <anchor>abb31f9070f1315cfb3f2c528e0ab1201</anchor>
      <arglist>(const Glib::SArray &amp;set)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>change_set_commit</name>
      <anchorfile>classGnome_1_1Conf_1_1Client.html</anchorfile>
      <anchor>a637924bcaf613156f2abb40914a8b3f1</anchor>
      <arglist>(ChangeSet &amp;set, bool remove_commited)</arglist>
    </member>
    <member kind="function">
      <type>ChangeSet</type>
      <name>change_set_reverse</name>
      <anchorfile>classGnome_1_1Conf_1_1Client.html</anchorfile>
      <anchor>ad5809e4e842457e7f67ee2efaf7ea3ce</anchor>
      <arglist>(const ChangeSet &amp;set)</arglist>
    </member>
    <member kind="function">
      <type>Glib::SignalProxy2&lt; void, const Glib::ustring &amp;, const Value &amp; &gt;</type>
      <name>signal_value_changed</name>
      <anchorfile>classGnome_1_1Conf_1_1Client.html</anchorfile>
      <anchor>aca9fa01e85ebca6f14b89e2dd8a7d727</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>value_changed</name>
      <anchorfile>classGnome_1_1Conf_1_1Client.html</anchorfile>
      <anchor>aad70c3032da6bf53c00f1f589df8d7bb</anchor>
      <arglist>(const Glib::ustring &amp;key, const Value &amp;value)</arglist>
    </member>
    <member kind="function">
      <type>Glib::SignalProxy1&lt; void, const Glib::Error &amp; &gt;</type>
      <name>signal_error</name>
      <anchorfile>classGnome_1_1Conf_1_1Client.html</anchorfile>
      <anchor>a9a206872b198bb115b1136fea81c6de9</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>error</name>
      <anchorfile>classGnome_1_1Conf_1_1Client.html</anchorfile>
      <anchor>a3100a84531a915318ce83fde4706ac91</anchor>
      <arglist>(const Glib::Error &amp;error)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static Glib::RefPtr&lt; Client &gt;</type>
      <name>get_default_client</name>
      <anchorfile>classGnome_1_1Conf_1_1Client.html</anchorfile>
      <anchor>a7bb9f0f8b8e6351d74823f432a14b59b</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static Glib::RefPtr&lt; Client &gt;</type>
      <name>get_client_for_engine</name>
      <anchorfile>classGnome_1_1Conf_1_1Client.html</anchorfile>
      <anchor>a30015273b97fdeae6f3c0f4aaf75aebe</anchor>
      <arglist>(GConfEngine *engine)</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>on_value_changed</name>
      <anchorfile>classGnome_1_1Conf_1_1Client.html</anchorfile>
      <anchor>a5b7c2ecbd06e5db0b892f07885047a83</anchor>
      <arglist>(const Glib::ustring &amp;key, const Value &amp;value)</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>on_unreturned_error</name>
      <anchorfile>classGnome_1_1Conf_1_1Client.html</anchorfile>
      <anchor>a5d08ed907b817c2f37ed26d7d49bf8d3</anchor>
      <arglist>(const Glib::Error &amp;error)</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>on_error</name>
      <anchorfile>classGnome_1_1Conf_1_1Client.html</anchorfile>
      <anchor>a79f42d1932b6ba088e7092f5ec0da15b</anchor>
      <arglist>(const Glib::Error &amp;error)</arglist>
    </member>
    <member kind="function">
      <type>Glib::RefPtr&lt; Gnome::Conf::Client &gt;</type>
      <name>wrap</name>
      <anchorfile>classGnome_1_1Conf_1_1Client.html</anchorfile>
      <anchor>abf27c17240058ebb5f26cdc872af043e</anchor>
      <arglist>(GConfClient *object, bool take_copy=false)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Gnome::Conf::Entry</name>
    <filename>classGnome_1_1Conf_1_1Entry.html</filename>
    <member kind="function">
      <type></type>
      <name>Entry</name>
      <anchorfile>classGnome_1_1Conf_1_1Entry.html</anchorfile>
      <anchor>ad114619bca38852b3de7dffbd2a88dd7</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Entry</name>
      <anchorfile>classGnome_1_1Conf_1_1Entry.html</anchorfile>
      <anchor>acb8b3ff71bbb15443c8462c755da227e</anchor>
      <arglist>(GConfEntry *castitem, bool make_a_copy=false)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Entry</name>
      <anchorfile>classGnome_1_1Conf_1_1Entry.html</anchorfile>
      <anchor>a3de2bddbcc41727362b2fef57918a948</anchor>
      <arglist>(const Entry &amp;src)</arglist>
    </member>
    <member kind="function">
      <type>Entry &amp;</type>
      <name>operator=</name>
      <anchorfile>classGnome_1_1Conf_1_1Entry.html</anchorfile>
      <anchor>ac465ec0a12dbbef76dddc65aa7adec33</anchor>
      <arglist>(const Entry &amp;src)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~Entry</name>
      <anchorfile>classGnome_1_1Conf_1_1Entry.html</anchorfile>
      <anchor>ae9c6c5d1008603aaf64ed86db3ecd9f7</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>GConfEntry *</type>
      <name>gobj</name>
      <anchorfile>classGnome_1_1Conf_1_1Entry.html</anchorfile>
      <anchor>a3fa42bd6fa0d91dfb575b7ce3720ca5e</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>const GConfEntry *</type>
      <name>gobj</name>
      <anchorfile>classGnome_1_1Conf_1_1Entry.html</anchorfile>
      <anchor>af5c9cae6308c3c72f9fdf770b665f0ed</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>GConfEntry *</type>
      <name>gobj_copy</name>
      <anchorfile>classGnome_1_1Conf_1_1Entry.html</anchorfile>
      <anchor>a422303c9dc4f65e969f6f52ae3f47375</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Entry</name>
      <anchorfile>classGnome_1_1Conf_1_1Entry.html</anchorfile>
      <anchor>a7609a8edd0e4d7a517b3ef412b7c41c4</anchor>
      <arglist>(const Glib::ustring &amp;key, const Value &amp;value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set_value</name>
      <anchorfile>classGnome_1_1Conf_1_1Entry.html</anchorfile>
      <anchor>a16948ce03ea4316a544d0221479f2088</anchor>
      <arglist>(const Value &amp;val)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set_schema_name</name>
      <anchorfile>classGnome_1_1Conf_1_1Entry.html</anchorfile>
      <anchor>a553448046882baf304aed678b5a7a4d7</anchor>
      <arglist>(const Glib::ustring &amp;val)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set_is_default</name>
      <anchorfile>classGnome_1_1Conf_1_1Entry.html</anchorfile>
      <anchor>af4a423314936047289428dc5f8a3cac8</anchor>
      <arglist>(bool is_default=true)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set_is_writable</name>
      <anchorfile>classGnome_1_1Conf_1_1Entry.html</anchorfile>
      <anchor>ae4ba16d3579381c3f04008d8f049acb3</anchor>
      <arglist>(bool is_writable=true)</arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>get_value</name>
      <anchorfile>classGnome_1_1Conf_1_1Entry.html</anchorfile>
      <anchor>a488382ffe9fccf7366f81c270472d746</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>Glib::ustring</type>
      <name>get_schema_name</name>
      <anchorfile>classGnome_1_1Conf_1_1Entry.html</anchorfile>
      <anchor>a7feb24768fde3ccb53c5e16010a5f627</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>Glib::ustring</type>
      <name>get_key</name>
      <anchorfile>classGnome_1_1Conf_1_1Entry.html</anchorfile>
      <anchor>ab5751342311e01cd8a2f206fec5a9891</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>get_is_default</name>
      <anchorfile>classGnome_1_1Conf_1_1Entry.html</anchorfile>
      <anchor>abfc7378540dac39371dcc802926ab8fd</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>get_is_writable</name>
      <anchorfile>classGnome_1_1Conf_1_1Entry.html</anchorfile>
      <anchor>a4834b1f25e1ccf6737c4ec6ee6fd54fa</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>GConfEntry *</type>
      <name>gobject_</name>
      <anchorfile>classGnome_1_1Conf_1_1Entry.html</anchorfile>
      <anchor>a10e92d1c2861f4000ed66101846b00df</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>Gnome::Conf::Entry</type>
      <name>wrap</name>
      <anchorfile>classGnome_1_1Conf_1_1Entry.html</anchorfile>
      <anchor>a9521580eb36508285f4b58bf91cf18fa</anchor>
      <arglist>(GConfEntry *object, bool take_copy=false)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Gnome::Conf::Schema</name>
    <filename>classGnome_1_1Conf_1_1Schema.html</filename>
    <member kind="function">
      <type></type>
      <name>Schema</name>
      <anchorfile>classGnome_1_1Conf_1_1Schema.html</anchorfile>
      <anchor>a56e0056ead7366b1a39d7a2bf63770a7</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Schema</name>
      <anchorfile>classGnome_1_1Conf_1_1Schema.html</anchorfile>
      <anchor>aae34864602bc91d85dbca1f6da8c0c58</anchor>
      <arglist>(GConfSchema *castitem, bool make_a_copy=false)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Schema</name>
      <anchorfile>classGnome_1_1Conf_1_1Schema.html</anchorfile>
      <anchor>a7715ff23a91942b378cf793dab58fc02</anchor>
      <arglist>(const Schema &amp;src)</arglist>
    </member>
    <member kind="function">
      <type>Schema &amp;</type>
      <name>operator=</name>
      <anchorfile>classGnome_1_1Conf_1_1Schema.html</anchorfile>
      <anchor>a79b0ac48480b1408238a88626cb2b4fa</anchor>
      <arglist>(const Schema &amp;src)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~Schema</name>
      <anchorfile>classGnome_1_1Conf_1_1Schema.html</anchorfile>
      <anchor>a57b54b8c084dcfed625f24aa58439fcb</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>GConfSchema *</type>
      <name>gobj</name>
      <anchorfile>classGnome_1_1Conf_1_1Schema.html</anchorfile>
      <anchor>a6900c3e818c493a715951d03f36c6d88</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>const GConfSchema *</type>
      <name>gobj</name>
      <anchorfile>classGnome_1_1Conf_1_1Schema.html</anchorfile>
      <anchor>a63c1dd1ab7088ec0c8f310611acfb457</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>GConfSchema *</type>
      <name>gobj_copy</name>
      <anchorfile>classGnome_1_1Conf_1_1Schema.html</anchorfile>
      <anchor>ace1f3fc9a78fa77b2319b24c2c169649</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set_type</name>
      <anchorfile>classGnome_1_1Conf_1_1Schema.html</anchorfile>
      <anchor>ab226ae3971c4cf9858a5dfae1f674d53</anchor>
      <arglist>(ValueType type)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set_list_type</name>
      <anchorfile>classGnome_1_1Conf_1_1Schema.html</anchorfile>
      <anchor>a6a5a3afa06f6dd16987b55730738fe12</anchor>
      <arglist>(ValueType type)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set_car_type</name>
      <anchorfile>classGnome_1_1Conf_1_1Schema.html</anchorfile>
      <anchor>a0652880a9dcf485170937274fe8c5cc7</anchor>
      <arglist>(ValueType type)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set_cdr_type</name>
      <anchorfile>classGnome_1_1Conf_1_1Schema.html</anchorfile>
      <anchor>a08f6aa6c612f648dd673351f5b418182</anchor>
      <arglist>(ValueType type)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set_locale</name>
      <anchorfile>classGnome_1_1Conf_1_1Schema.html</anchorfile>
      <anchor>a4b33f10efbd2d6488984728eb8b2ae9a</anchor>
      <arglist>(const std::string &amp;locale)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set_short_desc</name>
      <anchorfile>classGnome_1_1Conf_1_1Schema.html</anchorfile>
      <anchor>a849637d9012576fa5f9792aa80a2a5ed</anchor>
      <arglist>(const Glib::ustring &amp;desc)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set_long_desc</name>
      <anchorfile>classGnome_1_1Conf_1_1Schema.html</anchorfile>
      <anchor>ab345fe896b626fe7fa003323bd446ade</anchor>
      <arglist>(const Glib::ustring &amp;desc)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set_owner</name>
      <anchorfile>classGnome_1_1Conf_1_1Schema.html</anchorfile>
      <anchor>a8e534b114a8474d107a1cbd0482fdce2</anchor>
      <arglist>(const Glib::ustring &amp;owner)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set_default_value</name>
      <anchorfile>classGnome_1_1Conf_1_1Schema.html</anchorfile>
      <anchor>a328396689ae258fd4e1e21184a97a8b4</anchor>
      <arglist>(const Value &amp;value)</arglist>
    </member>
    <member kind="function">
      <type>ValueType</type>
      <name>get_type</name>
      <anchorfile>classGnome_1_1Conf_1_1Schema.html</anchorfile>
      <anchor>ab329ae7a1db706b42df76d051fbd13b4</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>ValueType</type>
      <name>get_list_type</name>
      <anchorfile>classGnome_1_1Conf_1_1Schema.html</anchorfile>
      <anchor>ac3f3f947f11a47be075a9f85b37a2b97</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>ValueType</type>
      <name>get_car_type</name>
      <anchorfile>classGnome_1_1Conf_1_1Schema.html</anchorfile>
      <anchor>aa7a6ded0e86e3e94aee976aafda52d89</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>ValueType</type>
      <name>get_cdr_type</name>
      <anchorfile>classGnome_1_1Conf_1_1Schema.html</anchorfile>
      <anchor>ab332b8b3a819507dad516e3f4ec96b29</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>std::string</type>
      <name>get_locale</name>
      <anchorfile>classGnome_1_1Conf_1_1Schema.html</anchorfile>
      <anchor>a878680d76f0fc0b6cf8267262d0b1015</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>Glib::ustring</type>
      <name>get_short_desc</name>
      <anchorfile>classGnome_1_1Conf_1_1Schema.html</anchorfile>
      <anchor>a9fb241d2be2c811d0905e86118ac89a4</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>Glib::ustring</type>
      <name>get_long_desc</name>
      <anchorfile>classGnome_1_1Conf_1_1Schema.html</anchorfile>
      <anchor>a70a94466a1adbfafec9df2c49d4bf062</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>Glib::ustring</type>
      <name>get_owner</name>
      <anchorfile>classGnome_1_1Conf_1_1Schema.html</anchorfile>
      <anchor>a3d736e0e5f3012a5d6601cb7a11c2fcb</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>get_default_value</name>
      <anchorfile>classGnome_1_1Conf_1_1Schema.html</anchorfile>
      <anchor>af7ca41609a9566d65c905d6f8abceb76</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>GConfSchema *</type>
      <name>gobject_</name>
      <anchorfile>classGnome_1_1Conf_1_1Schema.html</anchorfile>
      <anchor>a8ebc53882c55fa73f529a13fc2710c06</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>Gnome::Conf::Schema</type>
      <name>wrap</name>
      <anchorfile>classGnome_1_1Conf_1_1Schema.html</anchorfile>
      <anchor>af218318b015e54fadc5be376ed176d7e</anchor>
      <arglist>(GConfSchema *object, bool take_copy=false)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Gnome::Conf::Value</name>
    <filename>classGnome_1_1Conf_1_1Value.html</filename>
    <member kind="function">
      <type></type>
      <name>Value</name>
      <anchorfile>classGnome_1_1Conf_1_1Value.html</anchorfile>
      <anchor>acb0c2f82bf9dd85f24598d292033bb0d</anchor>
      <arglist>(GConfValue *castitem, bool make_a_copy=false)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Value</name>
      <anchorfile>classGnome_1_1Conf_1_1Value.html</anchorfile>
      <anchor>a63e0eb1d6375c72a95b63dfd2fef14c7</anchor>
      <arglist>(const Value &amp;src)</arglist>
    </member>
    <member kind="function">
      <type>Value &amp;</type>
      <name>operator=</name>
      <anchorfile>classGnome_1_1Conf_1_1Value.html</anchorfile>
      <anchor>a0ee044d4a39026b8fdc83a8186b62775</anchor>
      <arglist>(const Value &amp;src)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~Value</name>
      <anchorfile>classGnome_1_1Conf_1_1Value.html</anchorfile>
      <anchor>a5ee8070008b29d6aa3adf5cb6518df56</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>GConfValue *</type>
      <name>gobj</name>
      <anchorfile>classGnome_1_1Conf_1_1Value.html</anchorfile>
      <anchor>a64a706d96cf56e45493d62af2de8e030</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>const GConfValue *</type>
      <name>gobj</name>
      <anchorfile>classGnome_1_1Conf_1_1Value.html</anchorfile>
      <anchor>a3ed04516a95e2f424e68f3bd7a25def3</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>GConfValue *</type>
      <name>gobj_copy</name>
      <anchorfile>classGnome_1_1Conf_1_1Value.html</anchorfile>
      <anchor>aa20c40a20038668847d808aa5f8138fe</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Value</name>
      <anchorfile>classGnome_1_1Conf_1_1Value.html</anchorfile>
      <anchor>a8c00daedf4f3316e6dcc5dce8b8d1fed</anchor>
      <arglist>(ValueType type=VALUE_INVALID)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set</name>
      <anchorfile>classGnome_1_1Conf_1_1Value.html</anchorfile>
      <anchor>aca4f8a7fb134f4504eec174b31ba75bc</anchor>
      <arglist>(gint val)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set</name>
      <anchorfile>classGnome_1_1Conf_1_1Value.html</anchorfile>
      <anchor>aaed2eaecb076294d4826c569dc1d7cea</anchor>
      <arglist>(gdouble val)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set</name>
      <anchorfile>classGnome_1_1Conf_1_1Value.html</anchorfile>
      <anchor>abacfef96aa8e3e88d2db5fc2d4dbd179</anchor>
      <arglist>(bool val)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set</name>
      <anchorfile>classGnome_1_1Conf_1_1Value.html</anchorfile>
      <anchor>a6af6c2114a0b3e7f2a2357fe70ca9e1a</anchor>
      <arglist>(const Schema &amp;sc)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set_car</name>
      <anchorfile>classGnome_1_1Conf_1_1Value.html</anchorfile>
      <anchor>a39e30c82fdab76881d8d411274aae74c</anchor>
      <arglist>(const Value &amp;car)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set_cdr</name>
      <anchorfile>classGnome_1_1Conf_1_1Value.html</anchorfile>
      <anchor>afa1ae6bc950e794370fdb7c6955e07ca</anchor>
      <arglist>(const Value &amp;cdr)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set</name>
      <anchorfile>classGnome_1_1Conf_1_1Value.html</anchorfile>
      <anchor>aa75ad8c830bfd6cd8c10f0fcd8a0a20a</anchor>
      <arglist>(const Glib::ustring &amp;val)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set_list_type</name>
      <anchorfile>classGnome_1_1Conf_1_1Value.html</anchorfile>
      <anchor>a057d5d8c8027990dabaf9fbae00a2537</anchor>
      <arglist>(ValueType type)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set_int_list</name>
      <anchorfile>classGnome_1_1Conf_1_1Value.html</anchorfile>
      <anchor>a8e4cbbfc2a605e853592da868c2eead4</anchor>
      <arglist>(const SListHandle_ValueInt &amp;list)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set_bool_list</name>
      <anchorfile>classGnome_1_1Conf_1_1Value.html</anchorfile>
      <anchor>a6f8b6313b7b487aa9264349ad9f98672</anchor>
      <arglist>(const SListHandle_ValueBool &amp;list)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set_float_list</name>
      <anchorfile>classGnome_1_1Conf_1_1Value.html</anchorfile>
      <anchor>a2f98753d0eff221d8eec0c2764656b9a</anchor>
      <arglist>(const SListHandle_ValueFloat &amp;list)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set_string_list</name>
      <anchorfile>classGnome_1_1Conf_1_1Value.html</anchorfile>
      <anchor>a215663f91d77cad35af3232d49a36b3a</anchor>
      <arglist>(const SListHandle_ValueString &amp;list)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set_schema_list</name>
      <anchorfile>classGnome_1_1Conf_1_1Value.html</anchorfile>
      <anchor>aee60d27cfad352d13d99cfecfdb190be</anchor>
      <arglist>(const SListHandle_ValueSchema &amp;list)</arglist>
    </member>
    <member kind="function">
      <type>ValueType</type>
      <name>get_type</name>
      <anchorfile>classGnome_1_1Conf_1_1Value.html</anchorfile>
      <anchor>a27c8d797c64256a594ac457e927cfa4e</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>ValueType</type>
      <name>get_list_type</name>
      <anchorfile>classGnome_1_1Conf_1_1Value.html</anchorfile>
      <anchor>a9c979507cd99c59592acf0f6099b14a7</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>get_int</name>
      <anchorfile>classGnome_1_1Conf_1_1Value.html</anchorfile>
      <anchor>ae713e70be42ee07b556e991ebc9c3ba5</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>get_bool</name>
      <anchorfile>classGnome_1_1Conf_1_1Value.html</anchorfile>
      <anchor>a7e45d55bacdcc7acf992d4cd0a2bce7d</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>get_float</name>
      <anchorfile>classGnome_1_1Conf_1_1Value.html</anchorfile>
      <anchor>abf9a318b91ce642cb83a9de77bc772cc</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>Glib::ustring</type>
      <name>get_string</name>
      <anchorfile>classGnome_1_1Conf_1_1Value.html</anchorfile>
      <anchor>a150f2513c6baf8842f782c5a98ff135e</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>Schema</type>
      <name>get_schema</name>
      <anchorfile>classGnome_1_1Conf_1_1Value.html</anchorfile>
      <anchor>a2ed9e0bbd8f490d079c459bb3c384569</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>get_car</name>
      <anchorfile>classGnome_1_1Conf_1_1Value.html</anchorfile>
      <anchor>aa8887b5281653902c092d9b047295271</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>Value</type>
      <name>get_cdr</name>
      <anchorfile>classGnome_1_1Conf_1_1Value.html</anchorfile>
      <anchor>a2ff294a59591c01192d49ce53e968a9d</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>SListHandle_ValueFloat</type>
      <name>get_float_list</name>
      <anchorfile>classGnome_1_1Conf_1_1Value.html</anchorfile>
      <anchor>a4a262a2aa81835cbe3860cd127cdfd74</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>SListHandle_ValueInt</type>
      <name>get_int_list</name>
      <anchorfile>classGnome_1_1Conf_1_1Value.html</anchorfile>
      <anchor>a30205c3f2ab0fde1f8d6ab4964d2408f</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>SListHandle_ValueBool</type>
      <name>get_bool_list</name>
      <anchorfile>classGnome_1_1Conf_1_1Value.html</anchorfile>
      <anchor>a34e039c984e46a4897f4ee0029dfe209</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>SListHandle_ValueString</type>
      <name>get_string_list</name>
      <anchorfile>classGnome_1_1Conf_1_1Value.html</anchorfile>
      <anchor>ad5ea7e8fd838b810135f0d6f19858ddc</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>SListHandle_ValueSchema</type>
      <name>get_schema_list</name>
      <anchorfile>classGnome_1_1Conf_1_1Value.html</anchorfile>
      <anchor>adc1d8390cbcec05ced2e317181b873f0</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>Glib::ustring</type>
      <name>to_string</name>
      <anchorfile>classGnome_1_1Conf_1_1Value.html</anchorfile>
      <anchor>a5c0cc371ff9d8a3fca58b0fe23dd7741</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>GConfValue *</type>
      <name>gobject_</name>
      <anchorfile>classGnome_1_1Conf_1_1Value.html</anchorfile>
      <anchor>a3e545e5eab3bb4c21281aede72376c3b</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>Gnome::Conf::Value</type>
      <name>wrap</name>
      <anchorfile>classGnome_1_1Conf_1_1Value.html</anchorfile>
      <anchor>ac7ccdd8cc2e19e77705b7c41c6bcd151</anchor>
      <arglist>(GConfValue *object, bool take_copy=false)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Gnome::Conf::ChangeSet</name>
    <filename>classGnome_1_1Conf_1_1ChangeSet.html</filename>
    <base>Gnome::Conf::SetInterface</base>
    <member kind="typedef">
      <type>sigc::slot&lt; void, const Glib::ustring &amp;, const Value &amp; &gt;</type>
      <name>ForeachSlot</name>
      <anchorfile>classGnome_1_1Conf_1_1ChangeSet.html</anchorfile>
      <anchor>a7d1388fee2ca07f8bd1e3c6572b74466</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>ChangeSet</name>
      <anchorfile>classGnome_1_1Conf_1_1ChangeSet.html</anchorfile>
      <anchor>ac8512d100cd23ba25a9c03de99fc826e</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>ChangeSet</name>
      <anchorfile>classGnome_1_1Conf_1_1ChangeSet.html</anchorfile>
      <anchor>a5494cc1b496a730a018f50066b26ae85</anchor>
      <arglist>(GConfChangeSet *castitem, bool make_a_copy=false)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>ChangeSet</name>
      <anchorfile>classGnome_1_1Conf_1_1ChangeSet.html</anchorfile>
      <anchor>ac98c85e782b91625b5bf55b2c3840d33</anchor>
      <arglist>(const ChangeSet &amp;src)</arglist>
    </member>
    <member kind="function">
      <type>ChangeSet &amp;</type>
      <name>operator=</name>
      <anchorfile>classGnome_1_1Conf_1_1ChangeSet.html</anchorfile>
      <anchor>a54bd6f7c1fe704080962c98990275051</anchor>
      <arglist>(const ChangeSet &amp;src)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~ChangeSet</name>
      <anchorfile>classGnome_1_1Conf_1_1ChangeSet.html</anchorfile>
      <anchor>ae1e33583a1616ff419816bd0744f96a0</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>GConfChangeSet *</type>
      <name>gobj</name>
      <anchorfile>classGnome_1_1Conf_1_1ChangeSet.html</anchorfile>
      <anchor>a1cbbc631984905af6e939fc273181c69</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>const GConfChangeSet *</type>
      <name>gobj</name>
      <anchorfile>classGnome_1_1Conf_1_1ChangeSet.html</anchorfile>
      <anchor>ab711165f330643d911ff8a1d0a15b9b8</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>GConfChangeSet *</type>
      <name>gobj_copy</name>
      <anchorfile>classGnome_1_1Conf_1_1ChangeSet.html</anchorfile>
      <anchor>a959e5ce69f7de651a6ecfdbd3dd738df</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>clear</name>
      <anchorfile>classGnome_1_1Conf_1_1ChangeSet.html</anchorfile>
      <anchor>ab489ef15912d19e8478ccaa9a81a073f</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>unsigned int</type>
      <name>size</name>
      <anchorfile>classGnome_1_1Conf_1_1ChangeSet.html</anchorfile>
      <anchor>a4a69a72fd7aba31899334d6c70900be4</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>remove</name>
      <anchorfile>classGnome_1_1Conf_1_1ChangeSet.html</anchorfile>
      <anchor>af098c6cce537e9e81c6b779ed785ac22</anchor>
      <arglist>(const Glib::ustring &amp;key)</arglist>
    </member>
    <member kind="function">
      <type>Value *</type>
      <name>exists</name>
      <anchorfile>classGnome_1_1Conf_1_1ChangeSet.html</anchorfile>
      <anchor>a4d7e5dc246e8170d245ed770a397dac9</anchor>
      <arglist>(const Glib::ustring &amp;key) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>unset</name>
      <anchorfile>classGnome_1_1Conf_1_1ChangeSet.html</anchorfile>
      <anchor>a0f82dbd4e68e0affa07834e4d808806a</anchor>
      <arglist>(const Glib::ustring &amp;key)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>set</name>
      <anchorfile>classGnome_1_1Conf_1_1ChangeSet.html</anchorfile>
      <anchor>ac6a5be5034e8c3986428aeef15986c95</anchor>
      <arglist>(const Glib::ustring &amp;key, const Value &amp;value)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>set</name>
      <anchorfile>classGnome_1_1Conf_1_1ChangeSet.html</anchorfile>
      <anchor>ab5c732a86ea4448c4bc910821f11b338</anchor>
      <arglist>(const Glib::ustring &amp;key, bool what)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>set</name>
      <anchorfile>classGnome_1_1Conf_1_1ChangeSet.html</anchorfile>
      <anchor>a71a8d25523f7848b041876401bf7e773</anchor>
      <arglist>(const Glib::ustring &amp;key, int what)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>set</name>
      <anchorfile>classGnome_1_1Conf_1_1ChangeSet.html</anchorfile>
      <anchor>a51e79ca48f563691635d0f7edd8c4951</anchor>
      <arglist>(const Glib::ustring &amp;key, double what)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>set</name>
      <anchorfile>classGnome_1_1Conf_1_1ChangeSet.html</anchorfile>
      <anchor>a07ba2a7a58c731b86fa744ed0b5d9497</anchor>
      <arglist>(const Glib::ustring &amp;key, const Glib::ustring &amp;what)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>set</name>
      <anchorfile>classGnome_1_1Conf_1_1ChangeSet.html</anchorfile>
      <anchor>a3a29da545a5450d34b361a314e0518c7</anchor>
      <arglist>(const Glib::ustring &amp;key, const Schema &amp;what)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>for_each</name>
      <anchorfile>classGnome_1_1Conf_1_1ChangeSet.html</anchorfile>
      <anchor>a327e9704b57ca7b1aa61b3f519a11146</anchor>
      <arglist>(const ForeachSlot &amp;slot)</arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>GConfChangeSet *</type>
      <name>gobject_</name>
      <anchorfile>classGnome_1_1Conf_1_1ChangeSet.html</anchorfile>
      <anchor>a1d504b658cbef44303dc8e48ef1bffad</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Gnome::Conf::SetInterface</name>
    <filename>classGnome_1_1Conf_1_1SetInterface.html</filename>
    <member kind="function" virtualness="pure">
      <type>virtual void</type>
      <name>set</name>
      <anchorfile>classGnome_1_1Conf_1_1SetInterface.html</anchorfile>
      <anchor>aff29fa04ecbe3c81811b6129a9bec556</anchor>
      <arglist>(const Glib::ustring &amp;key, const Value &amp;value)=0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual void</type>
      <name>set</name>
      <anchorfile>classGnome_1_1Conf_1_1SetInterface.html</anchorfile>
      <anchor>a7e1003845faa0897722a491a738dc425</anchor>
      <arglist>(const Glib::ustring &amp;key, bool what)=0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual void</type>
      <name>set</name>
      <anchorfile>classGnome_1_1Conf_1_1SetInterface.html</anchorfile>
      <anchor>a9f3eb6b23df170f835771f9241d675ba</anchor>
      <arglist>(const Glib::ustring &amp;key, int what)=0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual void</type>
      <name>set</name>
      <anchorfile>classGnome_1_1Conf_1_1SetInterface.html</anchorfile>
      <anchor>aab09868d6718240f5999f4d647a22f72</anchor>
      <arglist>(const Glib::ustring &amp;key, double what)=0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual void</type>
      <name>set</name>
      <anchorfile>classGnome_1_1Conf_1_1SetInterface.html</anchorfile>
      <anchor>abfae1a040cc44b0f7293451cef652ff2</anchor>
      <arglist>(const Glib::ustring &amp;key, const Glib::ustring &amp;what)=0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual void</type>
      <name>set</name>
      <anchorfile>classGnome_1_1Conf_1_1SetInterface.html</anchorfile>
      <anchor>a27e9b88d39385f64d5e708007a438a34</anchor>
      <arglist>(const Glib::ustring &amp;key, const Schema &amp;what)=0</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set</name>
      <anchorfile>classGnome_1_1Conf_1_1SetInterface.html</anchorfile>
      <anchor>a3cceeeae136ccba0b41345c5f30cb96d</anchor>
      <arglist>(const Glib::ustring &amp;key, const ValuePair &amp;pair)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set_int_list</name>
      <anchorfile>classGnome_1_1Conf_1_1SetInterface.html</anchorfile>
      <anchor>a6fc7912b83799bacfc2d1db82c7c1def</anchor>
      <arglist>(const Glib::ustring &amp;key, const SListHandle_ValueInt &amp;list)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set_bool_list</name>
      <anchorfile>classGnome_1_1Conf_1_1SetInterface.html</anchorfile>
      <anchor>a3681db1d8a5fe79ba2fa80ac8db3bb8b</anchor>
      <arglist>(const Glib::ustring &amp;key, const SListHandle_ValueBool &amp;list)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set_float_list</name>
      <anchorfile>classGnome_1_1Conf_1_1SetInterface.html</anchorfile>
      <anchor>ac0d73403e9891941512132d3fd2e1e97</anchor>
      <arglist>(const Glib::ustring &amp;key, const SListHandle_ValueFloat &amp;list)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set_string_list</name>
      <anchorfile>classGnome_1_1Conf_1_1SetInterface.html</anchorfile>
      <anchor>aa726152c689325ab68a8535f748c20f4</anchor>
      <arglist>(const Glib::ustring &amp;key, const SListHandle_ValueString &amp;list)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set_schema_list</name>
      <anchorfile>classGnome_1_1Conf_1_1SetInterface.html</anchorfile>
      <anchor>abb2dbedc9718cd51aa18555655ebefe2</anchor>
      <arglist>(const Glib::ustring &amp;key, const SListHandle_ValueSchema &amp;list)</arglist>
    </member>
  </compound>
</tagfile>
