_CONVERSION(`GConfValue*',  `Value', `Value($3)')
_CONVERSION(`Value', `GConfValue*',  `($3).gobj()')
_CONVERSION(`const Value&', `const GConfValue*',  `($3).gobj()')
_CONVERSION(`GConfSchema*',  `Schema', `Schema($3)')
_CONVERSION(`Schema', `GConfSchema*',  `($3).gobj()')
_CONVERSION(`const Schema&', `const GConfSchema*',  `($3).gobj()')
_CONVERSION(`const Schema&', `GConfSchema*',  `const_cast<GConfSchema*>(($3).gobj())')
_CONVERSION(`GConfEntry*',  `Entry', `Entry($3)')
_CONVERSION(`GError*', `Glib::Error', `Glib::Error($3, true)')
_CONVERSION(`Glib::Error', `GError*', `($3).gobj()')
_CONVERSION(`GConfChangeSet*',  `ChangeSet', `ChangeSet($3)')
_CONVERSION(`ChangeSet&', `GConfChangeSet*',  `($3).gobj()')
_CONVERSION(`const ChangeSet&', `const GConfChangeSet*',  `($3).gobj()')
_CONVERSION(`const ChangeSet&', `GConfChangeSet*',  `const_cast<GConfChangeSet*>(($3).gobj())')

# This is purely for covenience. Some functions return a failure indicator, we 
# just throw an exception.
_CONVERSION(`gboolean',`void', `$3')

# Used by Signals:
# For some reason the compiler can't find Glib::unwrap_boxed().
_CONVERSION(`const Value&',`GConfValue*',`const_cast<GConfValue*>($3.gobj())')
_CONVERSION(`GConfValue*',`const Value&',`Value($3, true)')
_CONVERSION(`const Glib::Error&',`GError*',`const_cast<GError*>($3.gobj())')
_CONVERSION(`GError*',`const Glib::Error&',`Glib::Error($3, true)')

# Our Enumeration conversion
define(`_CONV_ENUM_GCONFMM',`dnl
_CONVERSION(`GConf$1', `$1', (($1)(__ARG3__)))
_CONVERSION(`GConf$1', `Gnome::Conf::$1', ((Gnome::Conf::$1)(__ARG3__)))
_CONVERSION(`$1', `GConf$1', ((GConf$1)(__ARG3__)))
_CONVERSION(`Gnome::Conf::$1', `GConf$1', ((GConf$1)(__ARG3__)))
')dnl
_CONV_ENUM_GCONFMM(`ClientPreloadType')
_CONV_ENUM_GCONFMM(`UnsetFlags')
_CONV_ENUM_GCONFMM(`ClientErrorHandlingMode')
_CONV_ENUM_GCONFMM(`ValueType')


# The GSList conversions
define(`__FL2H_DEEP',`$`'2($`'3, Glib::OWNERSHIP_DEEP)')
define(`__FL2H_SHALLOW',`$`'2($`'3, Glib::OWNERSHIP_SHALLOW)')
define(`__FL2H_NONE',`$`'2($`'3, Glib::OWNERSHIP_NONE)')
_CONVERSION(`GSList*',`Glib::SListHandle<Entry>',__FL2H_SHALLOW)
_CONVERSION(`GSList*',`Glib::SListHandle<Glib::ustring>',__FL2H_DEEP)
# These are for GConfValues, which are optimized to reduce copies
_CONVERSION(`GSList*',`SListHandle_ValueString',__FL2H_NONE)
_CONVERSION(`GSList*',`SListHandle_ValueSchema',__FL2H_NONE)
_CONVERSION(`GSList*',`SListHandle_ValueInt',__FL2H_NONE)
_CONVERSION(`GSList*',`SListHandle_ValueBool',__FL2H_NONE)
_CONVERSION(`GSList*',`SListHandle_ValueFloat',__FL2H_NONE)
_CONVERSION(`const SListHandle_ValueString&',`GSList*',`$3.data()')
_CONVERSION(`const SListHandle_ValueSchema&',`GSList*',`$3.data()')
_CONVERSION(`const SListHandle_ValueInt&',   `GSList*',`$3.data()')
_CONVERSION(`const SListHandle_ValueBool&',  `GSList*',`$3.data()')
_CONVERSION(`const SListHandle_ValueFloat&', `GSList*',`$3.data()')
# Arrays... ChangeSet needs this :-/
_CONVERSION(`const Glib::SArray&',`const gchar**',`const_cast<const char**>(($3).data())')

# This is purely for covenience. Some functions return a failure indicator, we 
# just throw an exception.
_CONVERSION(`gboolean',`void', `$3')

